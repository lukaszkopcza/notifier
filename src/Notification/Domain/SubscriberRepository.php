<?php

namespace App\Notification\Domain;

interface SubscriberRepository
{
    public function find(string $login): ?Subscriber;

    /**
     * @return Subscriber[]
     */
    public function findAll(): array;
}
