<?php


namespace App\Core\Messenger;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class InMemoryMessageBus implements MessageBusInterface
{
    private array $envelopes = [];

    public function dispatch($message, array $stamps = []): Envelope
    {
        $envelope =  $message instanceof Envelope ? $message : new Envelope($message, $stamps);
        $this->envelopes[] = $envelope;
        return $envelope;
    }

    public function getEnvelopes(): array
    {
        return $this->envelopes;
    }
}