<?php


namespace App\Notification\Domain\Sender;


use App\Notification\Domain\ValueObject\ContactChannel;

class NotificationSenderMap
{
    private array $senders;

    /**
     * NotificationSenderMap constructor.
     * @param SenderInterface[] $senders
     */
    public function __construct(SenderInterface ...$senders)
    {
        $this->senders = $senders;
    }

    /**
     * @param ContactChannel ...$channels
     * @return SenderInterface[]
     */
    public function getSendersForChannels(ContactChannel ...$channels): array
    {
        $senders = [];
        foreach ($channels as $channel) {
            foreach ($this->senders as $sender) {
                if ($sender->supports($channel)) {
                    $senders[(string) $channel] = $sender;
                }
            }
        }

        return $senders;
    }
}