<?php


namespace Tests\Notification\Integration;


use App\Core\Messenger\CommandBus;
use App\Notification\Application\Command\SendNotification;
use App\Notification\Domain\ValueObject\ContactChannel;
use Munus\Control\Either;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class SendingNotificationToChatTest extends KernelTestCase
{
    private CommandBus $commandBus;

    protected function setUp(): void
    {
        $container = static::getContainer();
        $this->commandBus = $container->get(CommandBus::class);
    }

    /**
     * @throws \Throwable
     * @dataProvider getMessages
     */
    public function testShouldSendNotificationToChat(string $login, string $message)
    {
        // Given
        $command = new SendNotification($login, $message);
        $command->setChannels(ContactChannel::CHAT());

        // When
       $result = $this->commandBus->dispatch($command);

        // Then
        $this->assertCount(1, array_filter($result->last(HandledStamp::class)->getResult(), fn (Either $result) => $result->isRight()));
    }

    public function getMessages()
    {
        $faker = \Faker\Factory::create();
        $messages = [];
        for ($i=0; $i < 50; $i++) {
            $messages[] = [
                $faker->randomElement(['jan.kowalski', 'andrzej.kowalski']),
                $faker->sentence
            ];
        }

        return $messages;
    }

}