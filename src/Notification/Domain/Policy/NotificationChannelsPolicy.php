<?php


namespace App\Notification\Domain\Policy;
use App\Notification\Domain\ValueObject\ContactChannel;

interface NotificationChannelsPolicy
{
    /**
     * @param ContactChannel ...$channels
     * @return ContactChannel[]
     */
    public function apply(ContactChannel ...$channels): array;
}