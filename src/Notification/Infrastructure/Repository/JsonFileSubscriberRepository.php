<?php


namespace App\Notification\Infrastructure\Repository;

use App\Notification\Domain\Subscriber;
use App\Notification\Domain\SubscriberRepository;
use App\Notification\Domain\ValueObject\ContactChannel;
use Safe\Exceptions\FilesystemException;
use Safe\Exceptions\JsonException;

class JsonFileSubscriberRepository implements SubscriberRepository
{
    private string $filePath;

    public function __construct(string $dbJsonfilePath)
    {
        $this->filePath = $dbJsonfilePath;
    }

    public function find(string $login): ?Subscriber
    {
        foreach ($this->findAll() as $notificationConfig) {
            if ($login === $notificationConfig->getLogin()) {
                return $notificationConfig;
            }
        }
        return null;
    }

    /**
     * @return Subscriber[]
     */
    public function findAll(): array
    {
        try {
            $json = \safe\file_get_contents($this->filePath);
        } catch (FilesystemException $e) {
            throw new \UnexpectedValueException('Unable to read json file', $e->getCode(), $e);
        }
        try {
            $records = \safe\json_decode($json, true);
        } catch (JsonException $e) {
            throw new \UnexpectedValueException('Unable to decode json file', $e->getCode(), $e);
        }
        return array_map([$this, 'mapToEntity'], $records);
    }

    /**
     * @param mixed[] $data
     * @return Subscriber
     */
    private function mapToEntity(array $data): Subscriber
    {
        $channels = null;
        if (null !== $data['contact_channels']) {
            $channels = array_map(
                function (string $channel) {
                    return new ContactChannel($channel);
                },
                explode(',', $data['contact_channels'])
            );
        }
        $subscriber = new Subscriber($data['login'], $data['email'], $data['phone']);
        if (!!$channels) {
            $subscriber->setChannels(...$channels);
        }

        return $subscriber;
    }
}
