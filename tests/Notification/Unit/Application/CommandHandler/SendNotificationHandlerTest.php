<?php


namespace Tests\Notification\Unit\Application\CommandHandler;


use App\Core\Messenger\InMemoryMessageBus;
use App\Notification\Application\Command\SendNotification;
use App\Notification\Application\CommandHandler\SendNotificationHandler;
use App\Notification\Domain\Exception\UserNotFoundException;
use App\Notification\Domain\Policy\DefaultEmailNotificationChannelPolicy;
use App\Notification\Domain\Sender\NotificationSenderMap;
use App\Notification\Domain\Subscriber;
use App\Notification\Domain\ValueObject\ContactChannel;
use App\Notification\Infrastructure\Repository\InMemorySubscriberRepository;
use App\Notification\Infrastructure\Sender\InMemoryEmailSender;
use App\Notification\Infrastructure\Sender\InMemorySmsSender;
use PHPUnit\Framework\TestCase;

class SendNotificationHandlerTest extends TestCase
{

    private SendNotificationHandler $handlerUnderTest;
    private InMemorySubscriberRepository $subscriberRepository;
    private InMemoryEmailSender $inMemoryEmailSender;
    private InMemorySmsSender $inMemorySmsSender;
    private InMemoryMessageBus $inMemoryMessageBus;

    protected function setUp(): void
    {
        $this->subscriberRepository = new InMemorySubscriberRepository();
        $this->inMemoryEmailSender = new InMemoryEmailSender();
        $this->inMemorySmsSender = new InMemorySmsSender();
        $this->inMemoryMessageBus = new InMemoryMessageBus();
        $this->handlerUnderTest = new SendNotificationHandler(
            $this->subscriberRepository,
            new NotificationSenderMap($this->inMemoryEmailSender, $this->inMemorySmsSender),
            $this->inMemoryMessageBus,
            new DefaultEmailNotificationChannelPolicy()
        );
    }

    /**
     * @throws \Throwable
     */
    public function testShouldThrowUserNotFoundExceptionWhenTryingToSendNotificationToNotExistingUser()
    {
        // Exepcted
        $this->expectException(UserNotFoundException::class);

        // Given
        $handler = $this->handlerUnderTest;
        $command = new SendNotification('login_1', 'Test message');


        // When
        $handler($command);

    }

    /**
     * @throws \Throwable
     */
    public function testShouldSendOneNotificationWhenUserExistsAndHasOneSubscribedChannel()
    {
        // Given
        $handler = $this->handlerUnderTest;
        $command = new SendNotification('login_1', 'Test message');
        $this->createSubscriber('login_1', [ContactChannel::EMAIL()]);

        // When
        $handler($command);

        // Then
        $this->assertCount(1, $this->inMemoryEmailSender->getMessages());
        $this->assertCount(0, $this->inMemorySmsSender->getMessages());
    }

    /**
     * @throws \Throwable
     */
    public function testShouldSendTwoNotificationWhenUserExistsAndHasTwoSubscribedChannel()
    {
        // Given
        $handler = $this->handlerUnderTest;
        $command = new SendNotification('login_1', 'Test message');
        $this->createSubscriber('login_1', [ContactChannel::EMAIL(), ContactChannel::SMS()]);

        // When
        $handler($command);

        // Then
        $this->assertCount(1, $this->inMemoryEmailSender->getMessages());
        $this->assertCount(1, $this->inMemorySmsSender->getMessages());
        $this->assertCount(0, $this->inMemoryMessageBus->getEnvelopes());
    }

    /**
     * @throws \Throwable
     */
    public function testShouldEmailNotificationWhenUserHasNoSelectedChannels()
    {
        // Given
        $handler = $this->handlerUnderTest;
        $command = new SendNotification('login_1', 'Test message');
        $this->createSubscriber('login_1', []);

        // When
        $handler($command);

        // Then
        $this->assertCount(1, $this->inMemoryEmailSender->getMessages());
        $this->assertCount(0, $this->inMemorySmsSender->getMessages());
    }

    /**
     * @throws \Throwable
     */
    public function testShouldRetryFailedNotification()
    {
        // Given
        $handler = $this->handlerUnderTest;
        $command = new SendNotification('login_1', 'Test message');
        $this->createSubscriber('login_1', [ContactChannel::EMAIL(), ContactChannel::SMS()]);
        $this->inMemoryEmailSender->willFail();


        // When
        $handler($command);

        // Then
        $this->assertCount(0, $this->inMemoryEmailSender->getMessages());
        $this->assertCount(1, $this->inMemorySmsSender->getMessages());
        $this->assertCount(1, $this->inMemoryMessageBus->getEnvelopes());
    }

    /**
     * @throws \Throwable
     */
    public function testShouldForceSendingMessageToSpecifiedChannel()
    {
        // Given
        $handler = $this->handlerUnderTest;
        $command = new SendNotification('login_1', 'Test message');
        $command->setChannels(ContactChannel::EMAIL());
        $this->createSubscriber('login_1', [ContactChannel::EMAIL(), ContactChannel::SMS()]);


        // When
        $handler($command);

        // Then
        $this->assertCount(1, $this->inMemoryEmailSender->getMessages());
        $this->assertCount(0, $this->inMemorySmsSender->getMessages());
        $this->assertCount(0, $this->inMemoryMessageBus->getEnvelopes());
    }


    private function createSubscriber(string $login, array $channels): Subscriber
    {
        $subscriber = new Subscriber($login, $login . '@example.com', (string) rand(100, 999) . ' 000 000');
        $subscriber->setChannels(...$channels);
        $this->subscriberRepository->add($subscriber);
        return $subscriber;
    }

}