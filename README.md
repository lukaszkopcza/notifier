# INSTALLATION

```bash
docker-compose up -d
docker-compose exec php-fpm bash
composer install
```

# SEND NOTIFICATION VIA CLI
```bash
TODO
```

# SEND NOTIFICATION VIA API
```
TODO
```

# UNIT TEST
```
bin/phpunit
```