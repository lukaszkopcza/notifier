<?php


namespace App\Notification\Infrastructure\Repository;

use App\Notification\Domain\Subscriber;
use App\Notification\Domain\SubscriberRepository;

class InMemorySubscriberRepository implements SubscriberRepository
{
    /** @var Subscriber[] */
    private array $store = [];

    public function add(Subscriber $notificationConfig): void
    {
        $this->store[$notificationConfig->getLogin()] = $notificationConfig;
    }

    public function find(string $login): ?Subscriber
    {
        return $this->store[$login] ?? null;
    }

    public function findAll(): array
    {
        return $this->store;
    }
}
