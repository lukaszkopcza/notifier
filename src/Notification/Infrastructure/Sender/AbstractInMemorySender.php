<?php


namespace App\Notification\Infrastructure\Sender;


use App\Notification\Domain\Sender\SenderInterface;
use App\Notification\Domain\Subscriber;
use Munus\Control\Either;

abstract class AbstractInMemorySender implements SenderInterface
{
    private array $store = [];
    private bool $failing = false;

    public function send(Subscriber $subscriber, string $message): Either
    {
        if ($this->failing) {
            return Either::left('Coś nie pykło');
        }

        $this->store[$subscriber->getLogin()][] = $message;
        return Either::right('OK');
    }

    public function getMessages(): array
    {
        return $this->store;
    }

    public function willFail(): void
    {
        $this->failing = true;
    }
}