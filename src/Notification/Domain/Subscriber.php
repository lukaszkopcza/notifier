<?php

namespace App\Notification\Domain;

//use App\Notification\Domain\Policy\ChannelsPolicy;
use App\Notification\Domain\Policy\NotificationChannelsPolicy;
use App\Notification\Domain\ValueObject\ContactChannel;

class Subscriber
{
    private string $login;
    private string $email;
    /** @var ContactChannel[] */
    private array $contactChannels = [];
    private string $phone;

    public function __construct(string $login, string $email, string $phone)
    {
        $this->login = $login;
        $this->email = $email;
        $this->phone = $phone;
    }

    public function setChannels(ContactChannel ...$channels): void
    {
        $this->contactChannels = $channels;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param NotificationChannelsPolicy $channelsPolicy
     * @return ContactChannel[]
     */
    public function getContactChannels(NotificationChannelsPolicy $channelsPolicy): array
    {
        return $channelsPolicy->apply(...$this->contactChannels);
    }
}
