<?php


namespace App\Notification\Domain\Sender;


use App\Notification\Domain\Subscriber;
use App\Notification\Domain\ValueObject\ContactChannel;
use Munus\Control\Either;

interface SenderInterface
{
    public function supports(ContactChannel $channel): bool;
    public function send(Subscriber $subscriber, string $message): Either;
}