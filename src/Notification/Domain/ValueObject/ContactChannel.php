<?php


namespace App\Notification\Domain\ValueObject;

use MyCLabs\Enum\Enum;

/**
 * Class ContactChannel
 * @package App\Notification\Domain\ValueObject
 * @method static ContactChannel EMAIL()
 * @method static ContactChannel SMS()
 * @method static ContactChannel CHAT()
 */
class ContactChannel extends Enum implements \Stringable
{
    private const EMAIL = 'email';
    private const SMS = 'sms';
    private const CHAT = 'chat';


    public function __toString()
    {
        return $this->value;
    }
}
