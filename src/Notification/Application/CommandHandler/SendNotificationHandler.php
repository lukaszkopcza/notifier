<?php


namespace App\Notification\Application\CommandHandler;


use App\Notification\Application\Command\SendNotification;
use App\Notification\Domain\Exception\UserNotFoundException;
use App\Notification\Domain\Policy\NotificationChannelsPolicy;
use App\Notification\Domain\Policy\UndefinedNotificationChannelsPolicy;
use App\Notification\Domain\Sender\NotificationSenderMap;
use App\Notification\Domain\Subscriber;
use App\Notification\Domain\SubscriberRepository;
use App\Notification\Domain\ValueObject\ContactChannel;
use Munus\Control\Either;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;

class SendNotificationHandler
{
    private SubscriberRepository $subscriberRepository;
    private NotificationSenderMap $notificationSenderMap;
    private NotificationChannelsPolicy $policy;
    private MessageBusInterface $messageBus;

    public function __construct(
        SubscriberRepository $subscriberRepository,
        NotificationSenderMap $notificationSenderMap,
        MessageBusInterface $messageBus,
        NotificationChannelsPolicy $channelsPolicy = null)
    {
        $this->subscriberRepository = $subscriberRepository;
        $this->notificationSenderMap = $notificationSenderMap;
        $this->messageBus = $messageBus;
        $this->policy = $channelsPolicy ?? new UndefinedNotificationChannelsPolicy();
    }

    public function __invoke(SendNotification $command)
    {
        if (! $subscriber = $this->subscriberRepository->find($command->getLogin())) {
            throw new UserNotFoundException('User not found');
        }

        $channels = $this->resolveChannels($subscriber, $command);
        $senders = $this->notificationSenderMap->getSendersForChannels(...$channels);
        $results = $this->sendNotifications($senders, $subscriber, $command);

        $this->retryIfSomethingFailed($results, $command);

        return $results;
    }

    /**
     * @param Either[] $results
     * @param SendNotification $command
     */
    private function retryIfSomethingFailed(array $results, SendNotification $command): void
    {
        $failed = array_filter($results, function (Either $result) {
           return $result->isLeft();
        });

        if (0 === count($failed)) {
            return;
        }
        $retryMessage = new SendNotification($command->getLogin(), $command->getMessage());



        $retryMessage->setChannels(...array_map(function (string $channelName) {
            return new ContactChannel($channelName);
        }, array_keys($results)));
        $this->messageBus->dispatch($retryMessage, [new DelayStamp(500000)]);
    }

    /**
     * @param Subscriber $subscriber
     * @param SendNotification $command
     * @return ContactChannel[]
     */
    private function resolveChannels(Subscriber $subscriber, SendNotification $command): array
    {
        $channels = $command->getContactChannels();

        if (count($channels) > 0) {
            return $channels;
        }

        return $subscriber->getContactChannels($this->policy);
    }

    /**
     * @param array $senders
     * @param Subscriber|null $subscriber
     * @param SendNotification $command
     * @return array
     */
    private function sendNotifications(array $senders, ?Subscriber $subscriber, SendNotification $command): array
    {
        $results = [];
        foreach ($senders as $channelName => $sender) {
            $results[$channelName] = $sender->send($subscriber, $command->getMessage());
        }
        return $results;
    }
}