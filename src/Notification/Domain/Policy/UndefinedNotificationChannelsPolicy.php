<?php


namespace App\Notification\Domain\Policy;


use App\Notification\Domain\ValueObject\ContactChannel;

class UndefinedNotificationChannelsPolicy implements NotificationChannelsPolicy
{
    /**
     * @param ContactChannel ...$channels
     * @return ContactChannel[]
     */
    public function apply(ContactChannel ...$channels): array
    {
        return $channels;
    }
}