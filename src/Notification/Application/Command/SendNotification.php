<?php


namespace App\Notification\Application\Command;


use App\Notification\Domain\ValueObject\ContactChannel;

class SendNotification
{
    /**
     * @var ContactChannel[]
     */
    private array $contactChannels = [];

    public function __construct(
        private string $login,
        private string $message
    ) {}

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setChannels(ContactChannel ...$contactChannels)
    {
        $this->contactChannels = $contactChannels;
    }

    /**
     * @return ContactChannel[]
     */
    public function getContactChannels(): array
    {
        return $this->contactChannels;
    }
}