<?php


namespace App\Notification\Infrastructure\Sender;

use App\Notification\Domain\ValueObject\ContactChannel;

class InMemoryEmailSender extends AbstractInMemorySender
{
    public function supports(ContactChannel $channel): bool
    {
        return $channel->equals(ContactChannel::EMAIL());
    }
}