<?php


namespace App\Notification\Domain\Policy;


use App\Notification\Domain\ValueObject\ContactChannel;

class DefaultEmailNotificationChannelPolicy implements NotificationChannelsPolicy
{

    public function apply(ContactChannel ...$channels): array
    {
        if (count($channels) > 0) {
            return $channels;
        }

        return [ContactChannel::EMAIL()];
    }
}