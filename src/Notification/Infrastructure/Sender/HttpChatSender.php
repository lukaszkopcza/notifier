<?php


namespace App\Notification\Infrastructure\Sender;


use App\Notification\Domain\Sender\SenderInterface;
use App\Notification\Domain\Subscriber;
use App\Notification\Domain\ValueObject\ContactChannel;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Munus\Control\Either;

class HttpChatSender implements SenderInterface
{
    public function __construct(
        private Client $client
    ) {}

    public function supports(ContactChannel $channel): bool
    {
        return ContactChannel::CHAT()->equals($channel);
    }

    public function send(Subscriber $subscriber, string $message): Either
    {
        try {
            $this->client->post('/', ['json' => $this->prepareMessage($subscriber, $message)]);
        } catch (GuzzleException $e) {
            return Either::left($e->getMessage());
        }

        return Either::right('OK');
    }

    private function prepareMessage(Subscriber $subscriber, string $message): array
    {
        return [
            'type' => 'message.chat',
            'payload' => [
                'content' => $message,
                'user' => ['name' => ucfirst($subscriber->getLogin())]
            ],
            'context' => null,
            'receivers' => []
        ];
    }
}