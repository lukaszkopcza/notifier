<?php


namespace App\Notification\Infrastructure\Sender;


use App\Notification\Domain\Sender\SenderInterface;
use App\Notification\Domain\Subscriber;
use App\Notification\Domain\ValueObject\ContactChannel;

class InMemorySmsSender extends AbstractInMemorySender
{
    public function supports(ContactChannel $channel): bool
    {
        return $channel->equals(ContactChannel::SMS());
    }
}